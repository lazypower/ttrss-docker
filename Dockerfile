FROM alpine:3.8

ARG TTRSS_VERSION=18.8

RUN apk add --no-cache \
    curl \
    git \
    nginx \
    php7-apcu \
    php7-cli \
    php7-curl \
    php7-dom \
    php7-fileinfo \
    php7-fpm \
    php7-iconv \
    php7-intl \
    php7-json \
    php7-mbstring \
    php7-mcrypt \
    php7-mysqli \
    php7-mysqlnd \
    php7-pcntl \
    php7-pdo \
    php7-pdo_mysql \
    php7-pdo_pgsql \
    php7-posix \
    php7-pgsql \
	php7-session \
    postgresql-client 

WORKDIR /var/www
RUN rm -rf localhost \
    && git clone --depth=1 --branch $TTRSS_VERSION https://tt-rss.org/git/tt-rss.git tt-rss

WORKDIR /var/www/tt-rss
RUN cd themes \
	&& git clone https://github.com/levito/tt-rss-feedly-theme \
	&& mv tt-rss-feedly-theme/feedly.css . \
	&& mv tt-rss-feedly-theme/feedly . \
	&& rm -rf tt-rss-feedly-theme \
    && rm -rf /etc/nginx/conf.d/* \
    && mkdir -p /run/nginx

COPY conf/ttrss.conf /etc/nginx/conf.d/ttrss.conf
COPY conf/config.php /var/www/tt-rss/config.php
COPY bin/ttrss_entrypoint.sh /sbin/ttrss_entrypoint.sh
COPY bin/start_ttrss /sbin/start_ttrss

EXPOSE 80

ENV TTRSS_DB_HOST="postgres"
ENV TTRSS_DB_PORT="5432"
ENV TTRSS_DB_NAME="ttrss"
ENV TTRSS_DB_USER="ttrss"
ENV TTRSS_DB_PASS="ttrsspass"
ENV TTRSS_SELF_URL_PATH="http://localhost"

ENTRYPOINT ["ttrss_entrypoint.sh"]

CMD ["start_ttrss"]
