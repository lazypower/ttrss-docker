#!/bin/sh
set -e

if [ -z "$TTRSS_DB_HOST" ]; then
    echo >&2 'error: missing required TTRSS_DB_HOST environment variable'
    exit 1
fi
if [ -z "$TTRSS_DB_PORT" ]; then
    echo >&2 'error: missing required TTRSS_DB_PORT environment variable'
    exit 1
fi
if [ -z "$TTRSS_DB_NAME" ]; then
    echo >&2 'error: missing required TTRSS_DB_NAME environment variable'
    exit 1
fi
if [ -z "$TTRSS_DB_USER" ]; then
    echo >&2 'error: missing required TTRSS_DB_USER environment variable'
    exit 1
fi
if [ -z "$TTRSS_DB_PASS" ]; then
    echo >&2 'error: missing required TTRSS_DB_PASS environment variable'
    exit 1
fi
if [ -z "$TTRSS_SELF_URL_PATH" ]; then
    echo >&2 'error: missing required TTRSS_SELF_URL_PATH environment variable'
    exit 1
fi


sed -i "s/TTRSS_DB_HOST/${TTRSS_DB_HOST}/" /var/www/tt-rss/config.php
sed -i "s/TTRSS_DB_PORT/${TTRSS_DB_PORT}/" /var/www/tt-rss/config.php
sed -i "s/TTRSS_DB_NAME/${TTRSS_DB_NAME}/" /var/www/tt-rss/config.php
sed -i "s/TTRSS_DB_USER/${TTRSS_DB_USER}/" /var/www/tt-rss/config.php
sed -i "s/TTRSS_DB_PASS/${TTRSS_DB_PASS}/" /var/www/tt-rss/config.php
sed -i "s|TTRSS_SELF_URL_PATH|${TTRSS_SELF_URL_PATH}|" /var/www/tt-rss/config.php


_ensure_postgres_running() {
    #Checking postgres is running 
    timeout=60
    
    while ! PGPASSWORD=${TTRSS_DB_PASS} psql -p $TTRSS_DB_PORT -h $TTRSS_DB_HOST -U $TTRSS_DB_USER  $TTRSS_DB_NAME -l
    do
        timeout=$(($timeout -1))
        if [[ $timeout -eq 0 ]]; then
            echo -e "\nCould not connect to database server. Aborting..."
            exit 1
        fi
        echo -n "."
        sleep 1
    done
}

_has_been_seeded(){
    PGPASSWORD=${TTRSS_DB_PASS} psql -p $TTRSS_DB_PORT -h $TTRSS_DB_HOST -U $TTRSS_DB_USER  $TTRSS_DB_NAME -t -c 'SELECT id FROM ttrss_feeds';
}

echo "Waiting for postgres (${TTRSS_DB_USER}@${TTRSS_DB_HOST}:${TTRSS_DB_NAME})"
_ensure_postgres_running

set +e
_has_been_seeded
if [ $? != 0 ]; then
    echo 'Performing initial database migration...'
    PGPASSWORD=${TTRSS_DB_PASS} psql -p $TTRSS_DB_PORT -h $TTRSS_DB_HOST -U $TTRSS_DB_USER  $TTRSS_DB_NAME < /var/www/tt-rss/schema/ttrss_schema_pgsql.sql
fi

exec "$@"
